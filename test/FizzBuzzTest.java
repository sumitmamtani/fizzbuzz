import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FizzBuzzTest{
    @Test
    void shouldReturnFizz3(){
        FizzBuzz fizzbuzz = new FizzBuzz();

        String result = fizzbuzz.parse(3);

        assertEquals("Fizz",result);


    }
    @Test
    void shouldReturnBuzz5(){
        FizzBuzz fizzbuzz = new FizzBuzz();

        String result = fizzbuzz.parse(5);

        assertEquals("Buzz",result);


    }
    @Test
    void shouldReturnBuzz10(){
        FizzBuzz fizzbuzz = new FizzBuzz();

        String result = fizzbuzz.parse(10);

        assertEquals("Buzz",result);


    }

    @Test
    void shouldReturnFizzBuzz5and3(){
        FizzBuzz fizzbuzz = new FizzBuzz();

        String result = fizzbuzz.parse(15);

        assertEquals("FizzBuzz",result);
    }

}