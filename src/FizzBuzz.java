class FizzBuzz {

    String parse(int i) {
        if (i == 0) {
            return "0";
        }
        if (i % 15 == 0) {
            return "FizzBuzz";
        }
        if (i % 3 == 0) {
            return "Fizz";
        }
        if (i % 5 == 0) {
            return "Buzz";
        }


        return String.valueOf(i);
    }
}
